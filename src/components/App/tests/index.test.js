import React from 'react';
import { render } from '../../../utils/testing';
import App from '..';

describe('components | App | index', () => {
  it('should render component', () => {
    const { container } = render(<App />);
    expect(container).toMatchSnapshot();
  });
});
