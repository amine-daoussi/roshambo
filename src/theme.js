import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    computer: {
      main: '#DF7A57',
    },
    player: {
      main: '#3F51B5',
    },
    tie: {
      main: '#508C22',
    },
    isPlaying: {
      main: '#CFE8F2',
    },
  },
});
