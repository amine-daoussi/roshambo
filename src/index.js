import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const ROOT = <App />;

ReactDOM.render(ROOT, document.getElementById('root'));
