import React from 'react';
import PropTypes from 'prop-types';
import { render } from '@testing-library/react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import theme from '../../theme';

const AllProviders = ({ children }) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
);

AllProviders.propTypes = {
  children: PropTypes.element,
};

export const customRender = (ui, options) => render(ui, { wrapper: AllProviders, options });
export * from '@testing-library/react';
export { customRender as render };
